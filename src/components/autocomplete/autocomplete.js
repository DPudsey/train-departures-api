import React, { useState, useEffect, Fragment } from "react";
import axios from "axios";
import { format } from "date-fns";
import { API_URL, LOCALIZATION } from "../../global/utils/constants";
import Loader from "./../loader/loader";
import "./autocomplete.scss";

const Autocomplete = () => {
  const [searchInput, setSearchInput] = useState(null);
  const [stationData, setStationData] = useState([]);
  const [errorMessage, setErrorMessage] = useState(false);
  const [autocompleteHasResults, setAutocompleteHasResults] = useState(false);
  const [stationCode, setStationCode] = useState(null);
  const [trainTimes, setTrainTimes] = useState(null);
  const [loadingStations, setLoadingStations] = useState(false);

  // Query the stations API
  useEffect(() => {
    if (searchInput && searchInput !== "") {
      axios
        .get(`${API_URL}stations?name=${searchInput}`)
        .then(res => {
          const stations = res.data;
          setErrorMessage(false);
          setAutocompleteHasResults(true);
          setStationData(stations?.data);
        })
        .catch(error => {
          setErrorMessage(true);
          setErrorMessage(LOCALIZATION.autocomplete.errorMessage);
          console.log(error.response);
        });
    }
    if (searchInput === "") {
      setStationData([]);
      setErrorMessage(false);
    }
  }, [searchInput]);

  const handleInputChange = value => {
    setSearchInput(value);
  };

  const selectStation = code => {
    setStationCode(code);
    setAutocompleteHasResults(false);
  };

  // Query the invidual station API
  useEffect(() => {
    if (stationCode) {
      setLoadingStations(true);
      axios
        .get(`${API_URL}stations/${stationCode}`)
        .then(res => {
          const data = res.data;
          setErrorMessage(false);
          setLoadingStations(false);
          return setTrainTimes(data?.data);
        })
        .catch(error => {
          setErrorMessage(true);
          setErrorMessage(LOCALIZATION.autocomplete.errorMessage);
          setLoadingStations(false);
          console.log(error.response);
        });
    }
  }, [stationCode]);

  return (
    <div className="Autocomplete">
      <div className="AutocompleteSearch">
        <header className="AutocompleteSearch__TitleWrapper">
          <h1 className="AutocompleteSearch__Title">
            {LOCALIZATION.autocomplete.title}
          </h1>
        </header>
        <div className="AutocompleteSearch__InputWrapper">
          <input
            className="AutocompleteSearch__Input"
            type="text"
            placeholder={LOCALIZATION.autocomplete.placeholder}
            onChange={e => {
              handleInputChange(e.target.value);
            }}
          />
        </div>
      </div>
      {autocompleteHasResults && stationData?.length ? (
        <div className="AutocompleteResults">
          <ul className="AutocompleteResults__StationList">
            {stationData.map(station => {
              const { name, code } = station;
              return (
                <li className="AutocompleteResults__StationItem" key={code}>
                  <button
                    className="AutocompleteResults__StationButton"
                    onClick={() => selectStation(code)}
                  >
                    {name}
                  </button>
                </li>
              );
            })}
          </ul>
        </div>
      ) : null}
      {(loadingStations || (stationCode && trainTimes)) && (
        <div className="AutocompleteStations">
          {loadingStations ? (
            <Loader />
          ) : (
            <div className="AutocompleteStations__TrainStation">
              <header className="AutocompleteStations__StationHeader">
                <h6 className="AutocompleteStations__StationName">
                  {trainTimes.station_name}
                </h6>
                <p className="AutocompleteStations__Time">
                  {format(new Date(trainTimes.date), "PPPP")}
                </p>
              </header>
              {trainTimes.departures?.length ? (
                <Fragment>
                  <p className="AutocompleteStations__JourneyDirection">
                    {LOCALIZATION.global.outbound}
                  </p>
                  <ul className="AutocompleteStations__TimesList">
                    {trainTimes.departures.map(departure => {
                      const {
                        aimed_departure_time,
                        destination_name,
                        train_uid
                      } = departure;
                      return (
                        <li
                          className="AutocompleteStations__TimesItem"
                          key={train_uid}
                        >
                          <span className="AutocompleteStations__DepartureTime">
                            {aimed_departure_time}
                          </span>
                          <span>{LOCALIZATION.global.from}</span>
                          <span className="AutocompleteStations__Departing">
                            <strong>{trainTimes.station_name}</strong>
                          </span>
                          <span>{LOCALIZATION.global.to}</span>
                          <span className="AutocompleteStations__Arriving">
                            <strong>{destination_name}</strong>
                          </span>
                        </li>
                      );
                    })}
                  </ul>
                </Fragment>
              ) : (
                <div className="AutocompleteWarning">
                  <p className="AutocompleteWarning__Message">
                    {LOCALIZATION.autocomplete.noDepartures}
                  </p>
                </div>
              )}
            </div>
          )}
        </div>
      )}
      {errorMessage && (
        <div className="AutocompleteError">
          <p className="AutocompleteError__Message">{errorMessage}</p>
        </div>
      )}
    </div>
  );
};

export default Autocomplete;
