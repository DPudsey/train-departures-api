import React from "react";
import "./loader.scss";

const LoadItem = () => {
  return (
    <div className="Loader__Item">
      <div className="Loader__Lines">
        <div className="Loader__Text Loader__Text--Long" />
        <div className="Loader__Text Loader__Text--Long" />
        <div className="Loader__Text Loader__Text--Short" />
      </div>
    </div>
  );
};

const Loader = () => {
  return (
    <div className="Loader">
      <LoadItem />
      <LoadItem />
      <LoadItem />
    </div>
  );
};

export default Loader;
