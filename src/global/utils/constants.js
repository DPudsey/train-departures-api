export const API_URL =
  "http://savills-techtest-nwapi.eba-eammdiqd.eu-west-2.elasticbeanstalk.com/";

export const LOCALIZATION = {
  global: {
    to: "to",
    from: "from",
    outbound: "outbound"
  },
  autocomplete: {
    title: "Today's train departures",
    placeholder: "Type the name of your train station",
    errorMessage: "I'm sorry, there has been a technical error.",
    noDepartures: "There are no departures from this station today"
  }
};
