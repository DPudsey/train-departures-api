import React from "react";
import Autocomplete from "./../components/autocomplete/autocomplete";
import "./page-template.scss";

const PageTemplate = () => {
  return (
    <section className="PageTemplate">
      <Autocomplete />
    </section>
  );
};

export default PageTemplate;
